#ifndef FELIXBASE_QUEUE_HPP
#define FELIXBASE_QUEUE_HPP

#include "tbb/concurrent_queue.h"
#include "blockingconcurrentqueue.h"
#include "packetformat/block_format.hpp"

typedef felix::packetformat::block Block;

// namespace felixbaset = felix::base;
namespace felix { namespace base {

  class ConcurrentQueue {
    private:
      static const unsigned PUSH_QUEUE_BUFFER_SIZE = 256;
      unsigned push_queue_pos = 0;
      Block *push_queue_buffer[PUSH_QUEUE_BUFFER_SIZE];

      static const unsigned POP_QUEUE_BUFFER_SIZE = 256;
      unsigned pop_queue_pos = 0;
      unsigned pop_queue_items_available = 0;
      Block *pop_queue_buffer[POP_QUEUE_BUFFER_SIZE];

      moodycamel::BlockingConcurrentQueue<Block *> *queue;


    public:
      ConcurrentQueue(size_t size = 0) {
        if (size > 0) {
          queue = new moodycamel::BlockingConcurrentQueue<Block *>(size);
        }
        else {
          queue = new moodycamel::BlockingConcurrentQueue<Block *>();
        }
      }

      virtual ~ConcurrentQueue() {
        delete queue;
      }

      void push(Block *b) {
        queue->enqueue(b);
      }

      void push_buffered(Block *b) {
        push_queue_buffer[push_queue_pos] = b;
        push_queue_pos++;

        if (push_queue_pos == PUSH_QUEUE_BUFFER_SIZE) {
          queue->enqueue_bulk(push_queue_buffer, push_queue_pos);
          push_queue_pos = 0;
        }
      }

      void pop(Block **b) {
        queue->wait_dequeue(*b);
      }

      void pop_buffered(Block **b) {
        if (pop_queue_pos == pop_queue_items_available) {
          pop_queue_items_available = queue->wait_dequeue_bulk(pop_queue_buffer, POP_QUEUE_BUFFER_SIZE);
          pop_queue_pos = 0;
        }
        *b = pop_queue_buffer[pop_queue_pos];
        pop_queue_pos++;
      }

      int size() {
        return queue->size_approx();
      }
      
  };

}} // namespace felixbase


#endif