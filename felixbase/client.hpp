#pragma once

/*
 This header contains definitions and functions to be used by FELIX clients,
 either to receive data from FELIX hosts or send data to FELIX hosts.
 */


namespace felix
{
namespace base
{
  // TODO: Need a structure here defining a global elink id
  typedef uint64_t elink_t;

  /**
   * Header of data packets sent by FELIX to clients.
   */
  struct FromFELIXHeader
  {
    uint16_t length;
    uint16_t status;
    uint32_t elinkid : 6; // TODO: Need global elink id here
    uint32_t gbtid : 26;
  };


  /**
   * Header of data packets sent by clients to FELIX hosts.
   */
  struct ToFELIXHeader
  {
    uint32_t length;
    uint32_t reserved;
    elink_t elinkid;
  };

  struct BusyMessage
  {
      elink_t elink;
      uint64_t command;

      enum {
        XON,
        XOFF,
        BUSYON,
        BUSYOFF
      };
  };
}
}
